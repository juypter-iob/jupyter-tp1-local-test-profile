ARG BASE_IMAGE=jupyter/minimal-notebook:42f4c82a07ff
FROM ${BASE_IMAGE}

RUN pip install \
	'jupyterlab_iframe==0.2.2'

RUN conda install --quiet --yes \
	'jupyterlab-git==0.20.0' \
	'ujson==2.0.3' \
	'ipywidgets==7.5.1' \
	'ipympl==0.5.6' \
	'matplotlib-base==3.2.1' \
	'jupyterlab_latex==2.0.0' && \
	conda clean --all

# maybe a fix for interact?
RUN conda install --quiet --yes -c conda-forge \
    jupyter_contrib_nbextensions && \
	conda clean --all

RUN pip install \
	'git+https://github.com/stv0g/nbgitpuller@f735265f7b2a429a17a8fab70cfd3557f060640d' \
	'rwth-nb==0.1.2'

# Basic Backend extensions
RUN jupyter serverextension enable --sys-prefix --py \
	jupyterlab_iframe \
	jupyterlab_latex \
	nbgitpuller

# Basic Frontend extensions
RUN jupyter labextension install \
	@jupyterlab/hub-extension@2.0.3 \
	@jupyterlab/toc@4.0.0 \
	@jupyter-widgets/jupyterlab-manager@2 \
	jupyter-matplotlib@0.7.2 \
	jupyterlab-drawio@0.7.0 \
	jupyterlab_filetree@0.2.2 \
	jupyterlab_iframe@0.2.2 \
	jupyterlab-rwth@0.0.1 \
	@jupyterlab/latex@2.0.1
    
USER root

# Use RWTH Mirror
#RUN sed -i 's|http://archive.ubuntu.com|http://ftp.halifax.rwth-aachen.de|g' /etc/apt/sources.list

RUN apt-get update && \
	apt-get -y install \
		texlive-latex-recommended \
		openssh-client && \
	rm -rf /var/lib/apt/lists/*


# Julia installation
# Default values can be overridden at build time
# (ARGS are in lower case to distinguish them from ENV)
# Check https://julialang.org/downloads/
ARG julia_version="1.5.3"
# SHA256 checksum
ARG julia_checksum="f190c938dd6fed97021953240523c9db448ec0a6760b574afd4e9924ab5615f1"

# Julia dependencies
# install Julia packages in /opt/julia instead of $HOME
ENV JULIA_DEPOT_PATH=/opt/julia \
    JULIA_PKGDIR=/opt/julia \
    JULIA_VERSION="${julia_version}"

WORKDIR /tmp

# hadolint ignore=SC2046
RUN mkdir "/opt/julia-${JULIA_VERSION}" && \
    wget -q https://julialang-s3.julialang.org/bin/linux/x64/$(echo "${JULIA_VERSION}" | cut -d. -f 1,2)"/julia-${JULIA_VERSION}-linux-x86_64.tar.gz" && \
    echo "${julia_checksum} *julia-${JULIA_VERSION}-linux-x86_64.tar.gz" | sha256sum -c - && \
    tar xzf "julia-${JULIA_VERSION}-linux-x86_64.tar.gz" -C "/opt/julia-${JULIA_VERSION}" --strip-components=1 && \
    rm "/tmp/julia-${JULIA_VERSION}-linux-x86_64.tar.gz"
RUN ln -fs /opt/julia-*/bin/julia /usr/local/bin/julia

# Show Julia where conda libraries are \
RUN mkdir /etc/julia && \
    echo "push!(Libdl.DL_LOAD_PATH, \"$CONDA_DIR/lib\")" >> /etc/julia/juliarc.jl && \
    # Create JULIA_PKGDIR \
    mkdir "${JULIA_PKGDIR}" && \
    chown "${NB_USER}" "${JULIA_PKGDIR}" && \
    fix-permissions "${JULIA_PKGDIR}"

USER ${NB_USER}

# Add Julia packages.
ADD requirements.jl .
RUN julia requirements.jl

# move kernelspec out of home \
# Install IJulia as jovyan and then move the kernelspec out
# to the system share location. Avoids problems with runtime UID change not
# taking effect properly on the .local folder in the jovyan home dir.
# Install&precompile packages via requirements.jl
RUN mv "${HOME}/.local/share/jupyter/kernels/julia"* "${CONDA_DIR}/share/jupyter/kernels/" && \
    chmod -R go+rx "${CONDA_DIR}/share/jupyter" && \
    rm -rf "${HOME}/.local" && \
    fix-permissions "${JULIA_PKGDIR}" "${CONDA_DIR}/share/jupyter"

WORKDIR $HOME
ENV JUPYTER_ENABLE_LAB=yes



