using Pkg

Pkg.add("Interpolations")
Pkg.add("PhysicalConstants")
Pkg.add("DifferentialEquations")
Pkg.add("Unitful")
Pkg.add("CSV")
Pkg.add("IJulia")
Pkg.add("Interact")
Pkg.add("WebIO")
Pkg.add("Plots")
Pkg.add("PyPlot")
Pkg.add("LaTeXStrings")
Pkg.add("GR")
Pkg.add("WebIO")

Pkg.update()
Pkg.build()
Pkg.precompile()

using WebIO

try
    WebIO.install_jupyter_labextension()
catch
    println("WebIO Error")
end

# try
#     WebIO.install_jupyter_nbextension()
# catch
#     println("WebIO Error")
# end
exit()
